import sys
import math

# Survive the wrath of Kutulu
# Coded fearlessly by JohnnyYuge & nmahoude (ok we might have been a bit scared by the old god...but don't say anything)


class Shelter():

    def __init__(self, x, y, energy, cooldown):
        self.x = x
        self.y = y
        self.energy = energy
        self.cooldown = cooldown

    def cooling(self):
        if self.cooldown == 0:
            self.energy = 10
            self.cooldown = 50
        else:
            self.cooldown -= 1

    def energy_use(self):
        self.energy -= 1


map = []
shelters = []
width = int(raw_input())
height = int(raw_input())
for i in xrange(height):
    line = raw_input()
    map.append([])
    for ind, j in enumerate(line):
        map[i].append(j)
        if j == "U":
            shelters.append(Shelter(ind, i, 0, 50))
# sanity_loss_lonely: how much sanity you lose every turn when alone, always 3 until wood 1
# sanity_loss_group: how much sanity you lose every turn when near another player, always 1 until wood 1
# wanderer_spawn_time: how many turns the wanderer take to spawn, always 3 until wood 1
# wanderer_life_time: how many turns the wanderer is on map after spawning, always 40 until wood 1
sanity_loss_lonely, sanity_loss_group, wanderer_spawn_time, wanderer_life_time = [int(i) for i in raw_input().split()]

"""
Pseudocode
if in the line of sight of a slasher:
  move out of the line of sight
else if a wanderer is close:
  move away
else if a wanderer is close and another explorer is close:
  yell
else if a wanderer is really close:
  light
else if there is an active shelter:
  move towards the active shelter
else if there are explorers still left:
  move towards a fellow explorer
else if sanity < 175:
  plan

move:
  if I am moving away from an enemy:
    if it is a wanderer:
      Find a valid path that puts me farther away
    if it is a slasher:
      move out of the line of sight
  else:
    move towards the entity given
  VALIDATE EACH MOVE, ASSURING IT DOESN'T PUT YOU IN MORE DANGER.
"""


class Entity():
    def __init__(self, entity_type, id, x, y, param_0, param_1, param_2):
        self.entity_type = entity_type
        self.id = id
        self.x = x
        self.y = y
        self.param_0 = param_0
        self.param_1 = param_1
        self.param_2 = param_2

    def distance_to(self, other):
        return math.sqrt(math.pow((self.x - other.x), 2) + math.pow((self.y - other.y), 2))

    def direction_of(self, other):
        def sign(x): return x and (1, -1)[x < 0]
        dir_x = sign(other.x - self.x)
        dir_y = sign(other.y - self.y)
        return (dir_x,  dir_y)

    def find_movable_position(self, move, enemy):
        tmp_pos = (self.x, self.y)
        # if map[self.y+move[1]][self.x+move[0]] != "#":
        #     return (self.x+move[0], self.y+move[1])
        # print >> sys.stderr, "attempting to check %s" % str((self.x+x, self.y+y))
        n_move = self.move_checker((move[0], move[1]-1), enemy)
        s_move = self.move_checker((move[0], move[1]+1), enemy)
        e_move = self.move_checker((move[0]+1, move[1]), enemy)
        w_move = self.move_checker((move[0]-1, move[1]), enemy)
        if n_move is not None:
            tmp_pos = n_move
        if s_move is not None:
            tmp_pos = s_move
        if e_move is not None:
            tmp_pos = e_move
        if w_move is not None:
            tmp_pos = w_move
        return tmp_pos

    def move_checker(self, move, enemy):
        tmp = 0
        temp_ent = Entity(None, None, self.x+move[0], self.y+move[1], None, None, None)
        if temp_ent.x < 0 or temp_ent.y < 0 or temp_ent.x > len(map[0]) or temp_ent.y > len(map):
            return None
        if map[temp_ent.y][temp_ent.x] != "#":
            tmp_dis = temp_ent.distance_to(enemy)
            # print >> sys.stderr, tmp_dis
            if tmp_dis > tmp:
                tmp = tmp_dis
                return (temp_ent.x, temp_ent.y)


def update_shelters(me, explorers):
    for shelter in shelters:
        if shelter.energy == 0:
            shelter.cooling()
        else:
            if len(explorers) > 0:
                for explorer in explorers:
                    if (explorer.x, explorer.y) == (shelter.x, shelter.y) or (shelter.x, shelter.y) == (me.x, me.y):
                        shelter.energy_use()


def get_closests(me, entities):
    closests = []
    for entity in entities:
        if len(closests) > 0:
            if me.distance_to(closests[0]) > me.distance_to(entity):
                closests.insert(0, entity)
            else:
                closests.append(entity)
        else:
            closests.append(entity)
    return closests


def get_closest_shelters(me, shelters):
    active = []
    for shelter in shelters:
        if shelter.energy > 0:
            active.append(shelter)
    return get_closests(me, active)


def get_los_slashers(me, slashers):
    to_ret = []
    for slasher in slashers:
        if (slasher.x - me.x) == 0 or (slasher.y - me.y) == 0:
            if (slasher.x - me.x) == 0 and (slasher.y - me.y) == 0:
                to_ret.append(slasher)
                continue
            true_los = True
            if (slasher.x - me.x) == 0:
                for i in xrange(slasher.y - me.y):
                    if map[i][me.x] != "#":
                        continue
                    else:
                        true_los = False
                        break
            if (slasher.y - me.y) == 0:
                for i in xrange(slasher.x - me.x):
                    if map[me.y][i] != "#":
                        continue
                    else:
                        true_los = False
                        break
            if true_los:
                to_ret.append(slasher)
    return to_ret


def compute_score(me, entities):
    score = 0.0
    if len(entities) == 0:
        return score
    for entity in entities:
        score += me.distance_to(entity)
    return (score/float(len(entities)))


def valid_move(me, tmp, explorers, wanderers, shelters, slashers):
    if len(get_los_slashers(tmp, slashers)) > 0:
        return False
    elif len(shelters) > 0 and tmp.distance_to(shelters[0]) < me.distance_to(shelters[0]):
        return True
    elif len(wanderers) > 1 and tmp.distance_to(wanderers[0]) > me.distance_to(wanderers[0]):
        if tmp.distance_to(wanderers[1]) > me.distance_to(wanderers[1]):
            return True
        else:
            return False
    elif len(wanderers) > 0 and tmp.distance_to(wanderers[0]) > me.distance_to(wanderers[0]):
        return True
    return True


def test_move(me, movement, current_score, explorers, wanderers, shelters, slashers):
    tmp = Entity(None, None, me.x, me.y, None, None, None)
    tmp.x += movement[0]
    tmp.y += movement[1]
    reduction = 0
    if map[tmp.y][tmp.x] == "#":
        return -10
    if movement == (0, 0):
        reduction += -0.25
    else:
        if not valid_move(me, tmp, explorers, wanderers, shelters, slashers):
            reduction += -2
    explorer_score = compute_score(tmp, explorers)
    wanderer_score = compute_score(tmp, wanderers)
    shelter_score = compute_score(tmp, shelters)
    slashers_score = compute_score(tmp, slashers)
    test_score = (wanderer_score, slashers_score, shelter_score, explorer_score)
    return ((test_score[0]-current_score[0]) + (test_score[1]-current_score[1])) + ((current_score[2]-test_score[2]) + (current_score[3]-test_score[3])) + reduction


def move(me, explorers, wanderers, shelters, slashers):
    explorer_score = compute_score(me, explorers)
    wanderer_score = compute_score(me, wanderers)
    shelter_score = compute_score(me, shelters)
    slashers_score = compute_score(me, slashers)
    current_score = (wanderer_score, slashers_score, shelter_score, explorer_score)
    # want to maximize the wanderer and slasher scores while minimizing the shelter and explorer scores
    # check cardinal directions.

    n_points = test_move(me, (0, -1), current_score, explorers, wanderers, shelters, slashers)
    e_points = test_move(me, (1, 0), current_score, explorers, wanderers, shelters, slashers)
    s_points = test_move(me, (0, 1), current_score, explorers, wanderers, shelters, slashers)
    w_points = test_move(me, (-1, 0), current_score, explorers, wanderers, shelters, slashers)
    wait_points = test_move(me, (0, 0), current_score, explorers, wanderers, shelters, slashers)
    print >> sys.stderr, str([n_points,
                              e_points,
                              s_points,
                              w_points,
                              wait_points])
    winner = max([n_points,
                  e_points,
                  s_points,
                  w_points,
                  wait_points])
    if winner == n_points:
        return (0, -1)
    elif winner == e_points:
        return (1, 0)
    elif winner == s_points:
        return (0, 1)
    elif winner == w_points:
        return (-1, 0)
    else:
        return (0, 0)


def move_escape(me, closest_explorers, closest_wanderers, closest_active_shelters, closest_slashers):
    if len(los_slashers) > 0:
        print >> sys.stderr, "In Slasher Line of sight!"
        for y in range(-2, 3, 1):
            for x in range(-2, 3, 1):
                find_pos = me.find_movable_position((x, y), los_slashers[0])
                if len(get_los_slashers(Entity(None, None, find_pos[0], find_pos[1], None, None, None), closest_slashers)) == 0:
                    return (find_pos[0] - me.x, find_pos[1] - me.y)
    if me.distance_to(closest_wanderers[0]) < 4:
        direction = me.direction_of(closest_wanderers[0])
        to_move = (direction[0] * -1, direction[1] * -1)
        print >> sys.stderr, "Wanderer Approaching, attempting to move this amount: %s" % str(to_move)
        find_pos = me.find_movable_position(to_move, closest_wanderers[0])
        return (find_pos[0] - me.x, find_pos[1] - me.y)


# game loop
while True:

    entity_count = int(raw_input())  # the first given entity corresponds to your explorer
    me = None
    explorers = []
    wanderers = []
    slashers = []
    effects = []
    to_move = (0, 0)
    for i in xrange(entity_count):
        entity_type, id, x, y, param_0, param_1, param_2 = raw_input().split()
        id = int(id)
        x = int(x)
        y = int(y)
        param_0 = int(param_0)
        param_1 = int(param_1)
        param_2 = int(param_2)
        if i == 0:
            me = Entity(entity_type, id, x, y, param_0, param_1, param_2)
        else:
            if entity_type == "WANDERER":
                wanderers.append(Entity(entity_type, id, x, y, param_0, param_1, param_2))
            if entity_type == "EXPLORER":
                explorers.append(Entity(entity_type, id, x, y, param_0, param_1, param_2))
            if entity_type == "SLASHER":
                slashers.append(Entity(entity_type, id, x, y, param_0, param_1, param_2))
            if entity_type == "EFFECT":
                effects.append(Entity(entity_type, id, x, y, param_0, param_1, param_2))

    # print >> sys.stderr, "Wanderers: %s" % str(wanderers)
    # print >> sys.stderr, "Explorers: %s" % str(explorers)
    # print >> sys.stderr, "Slashers: %s" % str(slashers)

    update_shelters(me, explorers)

    closest_explorers = get_closests(me, explorers)
    closest_wanderers = get_closests(me, wanderers)
    closest_active_shelters = get_closest_shelters(me, shelters)
    los_slashers = get_los_slashers(me, slashers)
    closest_slashers = get_closests(me, slashers)

    if len(closest_wanderers) > 0 and (me.distance_to(closest_wanderers[0]) < 4 or len(los_slashers) > 0):
        to_move = move_escape(me, closest_explorers, closest_wanderers, closest_active_shelters, closest_slashers)
    else:
        to_move = move(me, closest_explorers, closest_wanderers, closest_active_shelters, closest_slashers)

    # print >> sys.stderr, str((closest_wanderer.x, closest_wanderer.y)) if closest_wanderer else None
    # print >> sys.stderr, str((closest_explorer.x, closest_explorer.y)) if closest_explorer else None
    # print >> sys.stderr, str((me.x, me.y)) if closest_explorer else None
    # print >> sys.stderr, str(me.distance_to(closest_wanderer)) if closest_wanderer else None
    # if los_slashers:
    #     direction = me.direction_of(los_slashers[0])
    #     to_move = (direction[0] * -1, direction[1] * -1)
    #     print >> sys.stderr, "I'm in the los of a slasher, attempting to move this amount: %s" % str(to_move)
    #     find_pos = me.find_movable_position(to_move, los_slashers[0], slasher=True)
    #     to_move = (find_pos[0] - me.x, find_pos[1] - me.y)
    # if closest_wanderer is not None and me.distance_to(closest_wanderer) < 2:
    #     print "LIGHT"
    # if closest_wanderer is not None and me.distance_to(closest_wanderer) < 4:
    #     direction = me.direction_of(closest_wanderer)
    #     to_move = (direction[0] * -1, direction[1] * -1)
    #     print >> sys.stderr, "Wanderer Approaching, attempting to move this amount: %s" % str(to_move)
    #     find_pos = me.find_movable_position(to_move, closest_wanderer)
    #     to_move = (find_pos[0] - me.x, find_pos[1] - me.y)
    # elif closest_active_shelter is not None:
    #     print >> sys.stderr, "No active shelters around me."
    #     to_move = (closest_active_shelter.x - me.x, closest_active_shelter.y - me.y)
    # elif closest_explorer is not None and me.distance_to(closest_explorer) > 2:
    #     print >> sys.stderr, "Mpving towards a fellow explorer."
    #     to_move = (closest_explorer.x - me.x, closest_explorer.y - me.y)

    # Write an action using print
    # To debug: print >> sys.stderr, "Debug messages..."

    print >> sys.stderr, to_move
    print >> sys.stderr, str((me.x, me.y))
    if ((me.x+to_move[0]), (me.y+to_move[1])) != (me.x, me.y):
        print "MOVE %s %s" % ((me.x+to_move[0]), (me.y+to_move[1]))
    elif me.param_0 < 175 and me.id not in [x.id for x in effects if x.param_2 == me.id] and me.param_1 != 0:
        print "PLAN"
    else:
        print "WAIT"
